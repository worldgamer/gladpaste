#pragma once

class IPanel
{
public:
    const char *GetName(unsigned int vguiPanel)
    {
        typedef const char *(__thiscall* tGetName)(void*, unsigned int);
        return VT::vfunc<tGetName>(this, 36)(this, vguiPanel);
    }

	void set_mouse_input_enabled( unsigned int vguiPanel, bool state )
	{
		typedef void ( __thiscall* mousehac )( PVOID, int, bool);
		return VT::vfunc<mousehac>( this, 32 )( this, vguiPanel, state );
	}

#ifdef GetClassName
#undef GetClassName
#endif
    const char *GetClassName(unsigned int vguiPanel)
    {
        typedef const char *(__thiscall* tGetClassName)(void*, unsigned int);
        return VT::vfunc<tGetClassName>(this, 37)(this, vguiPanel);
    }
};
